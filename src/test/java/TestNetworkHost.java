import network.NetworkHost;
import static org.junit.Assert.*;
import org.junit.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
public class TestNetworkHost {
    private NetworkHost host;
    private byte[] addressInBytes = { (byte)127, (byte)0, (byte)0, (byte)1 };

    @Test
    public void hostShouldBeAbleToSendToAnotherHost() {
        host = new NetworkHost(NetworkHost.CLIENT_PORT);
        try {
            host.send(InetAddress.getByAddress(addressInBytes), NetworkHost.CLIENT_PORT, "test");
        } catch (UnknownHostException e) {
            fail("Wrong address");
        }
        assertEquals("test", host.waitForIncoming());
    }

    @After
    public void tearDown() {
        host.stop();
    }
}
