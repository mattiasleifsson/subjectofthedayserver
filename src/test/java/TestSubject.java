import subject.Subject;
import static org.junit.Assert.*;
import org.junit.*;
import java.util.List;
import java.util.ArrayList;
import java.io.File;
public class TestSubject {
    private Subject subject;
    private List<String> testSubjects1;
    private List<String> testSubjects2;

    @Before
    public void setUp() {
        testSubjects1 = new ArrayList<>();
        testSubjects1.add("subject11");
        testSubjects1.add("subject12");
        testSubjects1.add("subject13");
        testSubjects2 = new ArrayList<>();
        testSubjects2.add("subject21");
        testSubjects2.add("subject22");
        testSubjects2.add("subject23");
    }

    @Test
    public void subjectShouldHaveGetMethod() {
        subject = new Subject(java.time.LocalDate.now(), "lists/testlist1");
        subject.get();
    }

    @Test
    public void getShouldReturnValidSubject() {
        subject = new Subject(java.time.LocalDate.now(), "lists/testlist1");
        assertTrue(testSubjects1.contains(subject.get()));
    }

    @Test
    public void getShouldDependOnFile() {
        subject = new Subject(java.time.LocalDate.now(), "lists/testlist1");
        assertTrue("Subject is not from testlist1" + subject.get(), testSubjects1.contains(subject.get()));
        subject = new Subject("lists/testlist2");
        assertTrue("Subject is not from testlist2" + subject.get(), testSubjects2.contains(subject.get()));
    }

    @Test
    public void getShouldNotRepeatUntilAllSubjectsHaveAppeared() {
        int size = testSubjects1.size();
        for (int i = 0; i < size; i++) {
            subject = new Subject(java.time.LocalDate.now().plusDays(i), "lists/testlist1");
            assertTrue(testSubjects1.contains(subject.get()));
            testSubjects1.remove(subject.get());
        }
    }

    @Test
    public void getShouldReturnSameSubjectForAGivenDayInList() {
            assertEquals(new Subject(java.time.LocalDate.now(), "lists/testlist1").get(),
                         new Subject(java.time.LocalDate.now(), "lists/testlist1").get());
    }

    @Test
    public void SubjectsInFilesShouldNeverBeOverwritten() {
        subject = new Subject(java.time.LocalDate.now(), "lists/testlist1");
        String correctGet = subject.get();
        new Subject(java.time.LocalDate.now().plusDays(5), "lists/testlist1");
        assertEquals(correctGet, new Subject(java.time.LocalDate.now(), "lists/testlist1").get()); 
    }

    @Test
    public void SubjectShouldReturnNiceMessageIfFileDoesNotExist() {
        subject = new Subject(java.time.LocalDate.now(), "lists/nonexistinglist");
        assertEquals("List does not exist", subject.get());
    }

    @Test
    public void AllSubjectShouldBeUsedBeforeRepeat() {
        int size = testSubjects1.size();
        for (int i = 0; i < size; i++) {
            subject = new Subject(java.time.LocalDate.now().plusDays(i), "lists/testlist1");
            assertTrue(testSubjects1.contains(subject.get()));
            System.out.println(subject.get());
            testSubjects1.remove(subject.get());
        }
        System.out.println(testSubjects1.size());
        assertTrue(testSubjects1.isEmpty());
    }

    @After
    public void tearDown() {
        new File("lists/testlist1_day").delete();
        new File("lists/testlist2_day").delete();
    }
}
