package network;
import java.net.DatagramPacket;
import subject.Subject;
public class Server extends NetworkHost {

    public Server() {
        super(NetworkHost.SERVER_PORT);
    }
    public void start() {
        while (true) {
            DatagramPacket received = waitForIncomingPacket();
            send(received.getAddress(), NetworkHost.CLIENT_PORT, new Subject("lists/" + getMessage(received)).get());
        }
    }

    public static void main(String[] args) {
        Server server = new Server();
        server.start();
    }
}
