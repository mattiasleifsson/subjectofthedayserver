package network;

import java.net.InetAddress;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Scanner;
import java.io.ByteArrayInputStream;
import java.io.IOException;
public class NetworkHost {
    public static final int SERVER_PORT = 10355;
    public static final int CLIENT_PORT = 10356;
    private DatagramSocket socket;
    public NetworkHost(int port) {
        try {
            socket = new DatagramSocket(port);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void stop() {
        socket.close();
    }

    public void send(InetAddress destination, int destinationPort, String message) {
        byte[] messageBytes = message.getBytes();
        DatagramPacket packetToSend = new DatagramPacket(
            messageBytes,
            messageBytes.length,
            destination,
            destinationPort);
        try {
            socket.send(packetToSend);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    protected DatagramPacket waitForIncomingPacket() {
        byte[] buffer = new byte[64];
        DatagramPacket incomingPacket = new DatagramPacket(
            buffer, buffer.length);
        try {
            socket.receive(incomingPacket);
        } catch (IOException e) {
            System.out.println(e);
        }
        return incomingPacket;
    }

    protected String getMessage(DatagramPacket packet) {
        StringBuilder message = new StringBuilder();
        Scanner scanner = new Scanner(new ByteArrayInputStream(packet.getData()));
        scanner.useDelimiter("");
        for (int i = 0; i < packet.getLength(); i++) {
            message.append(scanner.next());
        }
        scanner.close();
        return message.toString();
    }

    public String waitForIncoming() {
        return getMessage(waitForIncomingPacket());
    }
}
