package network;
import java.net.InetAddress;
import java.net.UnknownHostException;
public class Client extends NetworkHost {
    private final byte[] serverAddress = { (byte)127, (byte)0,(byte)0, (byte)1 };

    public Client() {
        super(NetworkHost.CLIENT_PORT);
    }
    public String sendAndWaitForResponse(String message) {
        try {
        send(InetAddress.getByAddress(serverAddress), NetworkHost.SERVER_PORT, message);
        } catch (UnknownHostException e) {
            System.out.println(e);
        }
        return waitForIncoming();
    }

    public static void main(String[] args) {
        Client client = new Client();
        System.out.println(client.sendAndWaitForResponse(args[0]));
    }
}
