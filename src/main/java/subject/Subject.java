package subject;
import java.time.LocalDate;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
public class Subject {
    private String path;
    private LocalDate date;
    public Subject(LocalDate date, String path) {
        this.path = path;
        this.date = date;
    }

    public Subject(String path) {
        this(LocalDate.now(), path);
        this.date = LocalDate.now();
    }

    public String get() {
        if (!fileExists(path)) {
            return "List does not exist";
        }

        List<String> validSubjects = load(path + "_day");
        if (!containsDate(validSubjects, date)) {
            save(path + "_day", createNewLinesWithDates(date));
            validSubjects = load(path + "_day");
        }
        String result = getSubjectForDate(validSubjects, date);
        return result;
    }

    private boolean fileExists(String path) {
        return new File(path).exists();
    }

    private boolean containsDate(List<String> list, LocalDate date) {
        for (String s : list) {
            if (s.startsWith(date.toString())) {
                return true;
            }
        }
        return false;
    }

    private String getSubjectForDate(List<String> list, LocalDate date) {
        for (String s : list) {
            if (s.startsWith(date.toString())) {
                return s.replace(date.toString(), "");
            }
        }
        return "";
    }

    private List<String> createNewLinesWithDates(LocalDate date) {
        List<String> result = new ArrayList<>();
        List<String> subjects = load(path);
        int daysToIncrement = 0;
        while (!subjects.isEmpty()) {
            result.add(date.plusDays(daysToIncrement).toString() + getAndRemoveRandom(subjects));
            daysToIncrement++;
        }
        return result;
    }

    private List<String> load(String path) {
        List<String> subjectsInFile = new ArrayList<>();
        Scanner scanner;
        try {
            File file = new File(path);
            file.createNewFile();
            scanner = new Scanner(file);
            while(scanner.hasNextLine()) {
                subjectsInFile.add(scanner.nextLine());
            }
            scanner.close();
        } catch(IOException e) {
            System.out.println(e);
        }
        return subjectsInFile;
    }

    private void save(String path, List<String> lines) {
        PrintWriter pw;
        try {
            File file = new File(path);
            file.createNewFile();
            pw = new PrintWriter(file);
            for (int i = 0; i < lines.size(); i++) {
                pw.println(lines.get(i));
            }
            pw.flush();
            pw.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    private String getAndRemoveRandom(List<String> list) {
        int random = (int)(Math.random() * list.size());
        String result = list.get(random);
        list.remove(random);
        return result;
    }
}
